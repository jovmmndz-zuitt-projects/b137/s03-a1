package b137.mendez.s03a1;

import java.util.Scanner;

public class Factorial {

    public static void main(String[] args) {
        System.out.println("Factorial\n");

        // Activity:
        // Create a Java program that accepts an integer and computes for
        // the factorial value and displays it to the console.

        Scanner appScanner = new Scanner(System.in);
        System.out.println("Input number.\n");
        int factor = appScanner.nextInt();
        int factorial = 1;
        for (int i = 1; i <= factor; i++) {
            factorial = factorial * i;
        }

        System.out.println("Factorial of " + factor + " is " + factorial);

    }
}
